// Navigation/Navigation.js
import { createStackNavigator, createAppContainer } from 'react-navigation'
import Intro from './screens/IntroScreen'
import SignUpScreen from './screens/SignUpScreen'
import HomeSignScreen from './screens/HomeSignScreen'
import ListMovieScreen from './screens/ListMovieScreen'
import LoadingSign from './screens/LoadingSign'
import LogingScreen from './screens/LogingScreen'

  const Applicationstack = createStackNavigator({
    Intro: {
      screen: Intro,
    },
    HomeSignScreen: {
      screen: HomeSignScreen,
    },
    SignUpScreen: {
      screen: SignUpScreen,
    },
    ListMovieScreen: {
      screen: ListMovieScreen,
    },
    LoadingSign: {
      screen: LoadingSign,
    },
    LogingScreen: {
      screen: LogingScreen,
    },
  },
    {
      // Change this content to hot reload directly in the desired screen
      initialRouteName: 'LoadingSign',
    });

export default createAppContainer(Applicationstack);