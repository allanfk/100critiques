// SignUp.js
import React from "react";
import { StyleSheet, View, TextInput, Text } from "react-native";
import { Button } from "native-base";
import firebase from '../Firebase';
//import * as firebase from 'firebase';

export default class SignUpScreen extends React.Component {
  constructor() {
    super();
    state = {
      username: "",
      password: "",
      email: "",
      errorMessage: null
    }
    //firebase.initializeApp(config);
  }
  
  onChangeText = (key, val) => {
    this.setState({ [key]: val });
  };
  signUp = async () => {
    const { username, password, email, phone_number } = this.state;
    
    /* Basic input check */
    const check = () => {
      console.log(email);
      // username
      if (username.length < 3) 
        throw new Error('Votre username doit contenir au moins 3 caractéres');
      // email
      let at = email.indexOf("@")
      let point = email.indexOf(".")
      if (at == false || point == false || point < at)
        throw new Error("Votre email n'est pas valide");
      // password
      if (username.length < 3) 
        throw new Error('Votre mot de passe doit contenir au moins 6 caractéres');
    }
    /* Get Firebase data */ 
    // const name = (arguments) => {
    //   console.log(firebase.database().ref("personnage"))
    //   var query = firebase.database().ref("personnage"
    //   );
    //   query.once("value", function(snapshot) {
    //     snapshot.forEach(function(child) {
    //       console.log(child.key, child.val());
    //     });
    //   })
    // }
    const signUp = () => {
      firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then(() => this.props.navigation.navigate('ListMovieScreen'))
      .catch(error => this.setState({ errorMessage: error.message }))
    }
    try {
      // here place your signup logic
      check();
      signUp();
      /*console.log("user successfully signed up!: ", success)*/;
    } catch (err) {
      console.log("error signing up: ", err);
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.input}
          placeholder="Username"
          autoCapitalize="none"
          placeholderTextColor="#b6b6b6"
          onChangeText={val => this.onChangeText("username", val)}
        />
        <TextInput
          style={styles.input}
          placeholder="Email"
          autoCapitalize="none"
          placeholderTextColor="#b6b6b6"
          onChangeText={val => this.onChangeText("email", val)}
        />
        <TextInput
          style={styles.input}
          placeholder="Password"
          secureTextEntry={true}
          autoCapitalize="none"
          placeholderTextColor="#b6b6b6"
          onChangeText={val => this.onChangeText("password", val)}
        />
        {/*
        <TextInput
          style={styles.input}
          placeholder='Phone Number'
          autoCapitalize="none"
          placeholderTextColor='white'
          onChangeText={val => this.onChangeText('phone_number', val)}
        />*/}
        <Button title="Sign Up"
        // bordered
        primary
        style={{
          padding: 15,
          width: 280,
          alignItems: "center",
          justifyContent: "center"
        }}
         onPress={this.signUp} >
         <Text style={{ color: "white", fontWeight: "bold" }}>
              Créer un compte
            </Text>
          </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  input: {
    width: 350,
    height: 55,
    backgroundColor: "white",
    color:"black",
    margin: 10,
    padding: 8,
    borderRadius: 14,
    fontSize: 18,
    fontWeight: "500"
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#0ad06f"
  }
});
