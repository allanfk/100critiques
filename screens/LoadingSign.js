import React from 'react'
import { View, Text, ActivityIndicator, StyleSheet } from 'react-native'
// import * as firebase from 'firebase';
import firebase from '../Firebase';



export default class LoadingSign extends React.Component {
  componentDidMount() {
    /* SignOut */
    firebase.auth().signOut().then(function() {
      console.log('Sign-out successful')
    }, function(error) {
      // An error happened.
    });

    firebase.auth().onAuthStateChanged(user => {
      this.props.navigation.navigate(user ? 'ListMovieScreen' : 'HomeSignScreen')
    })
  }
  render() {
    return (
      <View style={styles.container}>
        <Text>Loading</Text>
        <ActivityIndicator size="large" />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
})