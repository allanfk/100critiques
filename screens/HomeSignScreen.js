import React, { Component } from "react";
import { View, StyleSheet, Alert, Image } from "react-native";
import { Content, Button, Icon, Text } from "native-base";
import { Constants, Font } from "expo";
import FbAuth from "../components/FbAuth/FbAuth";

export default class HomeSignScreen extends Component {
  constructor(props) {
    super(props);
    this.state = { loading: true };
  }

  async componentWillMount() {
    await Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });
    this.setState({ loading: false });
  }

  render() {
    const { navigate } = this.props.navigation;
    if (this.state.loading) {
      return <Text>Lolloedlo</Text>;
    }
    return (
      <View style={styles.container}>
        <Button onPress={() => navigate("LogingScreen")}>
          <Text>
          Connexion
          </Text>
        </Button>
        <Image
          style={styles.image}
          source={require("../assets/images/senscritique.png")}
        />
        <Text style={styles.paragraph}>Bienvenue sur Senscritique</Text>
        <FbAuth />
        <Content>
          <Button
            bordered
            light
            style={{
              padding: 15,
              width: 280,
              alignItems: "center",
              justifyContent: "center"
            }}
            onPress={() => navigate("SignUpScreen")}
          >
            <Text style={{ color: "white", fontWeight: "bold" }}>
              Créer un compte
            </Text>
          </Button>
        </Content>

        <Text style={[styles.paragraph, styles.subpara]}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
          tempor velit augue, eget lacinia quam pellentesque in. Vestibulum enim
          nisl volutpat.
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    paddingTop: Constants.statusBarHeight,
    backgroundColor: "#0ad06f"
  },
  image: {
    height: 150,
    width: 230
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: "bold",
    textAlign: "center",
    color: "#ffffff"
  },
  subpara: {
    fontSize: 15
  }
});
