import React from 'react';
import { Ionicons } from '@expo/vector-icons';
import { StyleSheet, View, Text, Image, TouchableHighlight } from 'react-native';
import { LinearGradient } from 'expo';
import AppIntroSlider from 'react-native-app-intro-slider';

const styles = StyleSheet.create({
  mainContent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  image: {
    width: 230,
    height: 230,
    borderRadius: 400,
    borderWidth: 3,
    borderColor: '#ffffff',
  },
  text: {
    color: 'rgba(255, 255, 255, 0.8)',
    backgroundColor: 'transparent',
    textAlign: 'center',
    paddingHorizontal: 16,
    fontSize: 16,
    fontWeight: 'bold',
  },
  title: {
    color: 'white',
    backgroundColor: 'transparent',
    textAlign: 'center',
    marginBottom: 16,
    fontWeight: 'bold',
    fontSize: 20
  }
});

const slides = [
  {
    key: 'somethun',
    title: 'Voici Ted \n Ted est sur senscritique'.toUpperCase(),
    image: require('../assets/images/ted1.jpg'),
    colors: ['#0ad06f', '#0ad06f'],
  },
  {
    key: 'somethun2',
    title: 'TED EST INSPIRé'.toUpperCase(),
    text: 'Ted consulte des milliers de nouveaux avis chaque jour',
    image: require('../assets/images/ted_ordi.png'),
    colors: ['#0ad06f', '#0ad06f'],
  },
  {
    key: 'somethun3',
    title: "TED n'est jamais seul".toUpperCase(),
    text: 'Ted discute avec ses amis et tous ceux qui partagent ses passions',
    image: require('../assets/images/ted_friend2.jpg'),
    colors: ['#0ad06f', '#0ad06f'],
  },
  {
    key: 'somethun4',
    title: "TED n'oublie rien".toUpperCase(),
    text: "Ted met de coté tout ce qu'on lui conseille.",
    image: require('../assets/images/ted_paper.jpg'),
    colors: ['#0ad06f', '#0ad06f'],
  },
  {
    key: 'somethun5',
    title: "Tout le monde parle de Ted".toUpperCase(),
    text: "Les gens écoutent l'avis de Ted",
    image: require('../assets/images/ted_famous.jpg'),
    colors: ['#0ad06f', '#0ad06f'],
  },
  {
    key: 'somethun6',
    title: "TED est heureux \n Ted est sur sens critique".toUpperCase(),
    image: require('../assets/images/ted4.jpg'),
    colors: ['#0ad06f', '#0ad06f'],
  },
];

export default class Intro extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showRealApp: false
          }
      }

      _onDone = () => {
        // User finished the introduction. Show real app through
        // navigation or simply by controlling state
        // this.setState({ showRealApp: true });
        this.props.navigation.navigate('HomeSignScreen')
      }
      
  _renderItem = props => (
    <LinearGradient
      style={[styles.mainContent, {
        paddingTop: props.topSpacer,
        paddingBottom: props.bottomSpacer,
        width: props.width,
        height: props.height,
      }]}
      colors={props.colors}
      start={{x: 0, y: .1}} end={{x: .1, y: 1}}
    >
      <Ionicons style={{ backgroundColor: 'transparent' }} name={props.icon} size={200} color="white" />
      <TouchableHighlight onPress={() => {console.log('You tapped the button!');}}>
        <Image style={styles.image} source={props.image}/>
      </TouchableHighlight>
      <View>
        <Text style={styles.title}>{props.title}</Text>
        <Text style={styles.text}>{props.text}</Text>
      </View>
    </LinearGradient>
  );

  render() {
    return (
      <AppIntroSlider
        slides={slides}
        renderItem={this._renderItem}
        bottomButton
        onDone={this._onDone}
      />
    );
  }
}
